﻿#include "pch.h"
#define _CRT_SECURE_NO_WARNINGS
#include "clist.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

void printNode(elemtype* value) {
	printf("%d\n", *((int*)value));
}

int Menu() {
	int num;
	printf("1.Создание списка\n");
	printf("2.Добавление элементов в список\n");
	printf("3.Удаление элементов из списка\n");
	printf("4.Вывод списка на дисплей\n");
	printf("5.Удаление списка\n");
	printf("0.Выход\nВиберите раздел:");
	scanf("%d", &num);
	return num;
}

int MenuPush() {
	int num;
	printf("1.Добавить в начало списка\n");
	printf("2.Добавить в средину списка\n");
	printf("3.Добавить в конец списка\nВиберите раздел:");
	scanf("%d", &num);
	return num;
}

int MenuPop() {
	int num;
	printf("1.Удалить первый элемент\n");
	printf("2.Удалить элемент по индексу\n");
	printf("3.Удалить последний элемент\nВиберите раздел:");
	scanf("%d", &num);
	return num;
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	elemtype element;
	int num = 100;
	int fnum, snum;
	int index;
	bool f = false;
	cList* mylist = createList();
	while (num != 0) {
		num = Menu();
		switch (num) {
		case 1:
			printf("Список создан!\n");
			system("pause");
			system("cls");
			f = true;
			break;
		case 2:
			if (f == false) {
				printf("Ошибка! Создайте список, пожалуйста.\n");
				system("pause");
				system("cls");
			}
			else {
				fnum = MenuPush();
				switch (fnum) {
				case 1:
					printf("Введите элемент, который добавляется:");
					scanf("%d", &element);
					pushFront(mylist, &element);
					system("cls");
					break;
				case 2:
					printf("Введите индекс элемента, который добавляется:");
					scanf("%d", &index);
					printf("Введите элемент, который добавляеться:");
					scanf("%d", &element);
					pushMiddle(mylist, index, &element);
					system("cls");
					break;
				case 3:
					printf("Введите элемент, который добавляется:");
					scanf("%d", &element);
					pushBack(mylist, &element);
					system("cls");
					break;
				}
			}
				break;
			
		case 3:
			if (f == false) {
				printf("Ошибка! Создайте список, пожалуйста.\n");
				system("pause");
				system("cls");
			}
			else {
				snum = MenuPop();
				switch (snum) {
				case 1:
					popFront(mylist, &element);
					printf("Элемент удалён!");
					system("pause");
					system("cls");
					break;
				case 2:
					printf("Введите индекс элемента, Который удаляется:");
					scanf("%d", &index);
					popMiddle(mylist, index);
					printf("Элемент удалён!");
					system("pause");
					system("cls");
					break;
				case 3:
					popBack(mylist, &element);
					printf("Элемент удалён!");
					system("pause");
					system("cls");
					break;
				}
			}
			break;
		case 4:
			if (f == false) {
				printf("Ошибка! Создайте список, пожалуйста.\n");
				system("pause");
				system("cls");
			}
			else {
				system("cls");
				printList(mylist, printNode);
				system("pause");
				system("cls");
			}
			break;
		case 5:
			if (f == false) {
				printf("Ошибка! Создайте список, пожалуйста.\n");
				system("pause");
				system("cls");
			}
			else {
				deleteList(mylist);
				printf("Список удалён!");
				system("pause");
				system("cls");
			}
			break;
		}
	}
}

