﻿#include <Windows.h>
#include <stdio.h>

struct MyStruct
{
	unsigned char second;
	unsigned char minute;
	unsigned char hour;
	unsigned char day;
	unsigned char month;
	int year;
};
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	MyStruct t;
	do {
		printf("Введите секунды:");
		scanf_s("%hhu", &t.second);
		if (t.second < 1 || t.second>59)
			printf("Ошибка! Введите число от 1 до 59.\n");
	} while (t.second < 1 || t.second>59);
	do {
		printf("Введите минуты:");
		scanf_s("%hhu", &t.minute);
		if (t.minute > 59 || t.minute < 1)
			printf("Ошибка! Введите число от 1 до 59.\n");
	} while (t.minute > 59 || t.minute < 1);
	do {
		printf("Введите часы:");
		scanf_s("%hhu", &t.hour);
		if (t.hour < 1 || t.hour > 23)
			printf("Ошибка! Введите число от 1 до 23.\n");
	} while (t.hour < 1 || t.hour >23);

	printf("Введите день:");
	scanf_s("%hhu", &t.day);

	printf("Введите месяц:");
	scanf_s("%hhu", &t.month);
	printf("Введите год:");
	scanf_s("%d", &t.year);
	printf("%d:%d:%d  %d.%d.%d\n", t.hour, t.minute, t.second, t.day, t.month, t.year);
	printf("Обьём памяти, который занимает структура:%d\n", sizeof(MyStruct));
	return 0;
}
