﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>


union SignedShort
{
	signed short a;
	struct Bites {
		unsigned short value : 15;
		unsigned short sign : 1;
	}bites;
}A;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Введите число:");
	scanf_s("%d", &A);
	printf("Используем структуру данных:");
	if (A.bites.sign == 1)
		printf("Число отрицательное\n");
	else
		printf("Число положительное\n");
	printf("Используемлогическую операцию: ");
	if (A.a < 0)
		printf("Число отрицательное\n");
	else
		printf("Число положительное\n");
	return 0;
}
