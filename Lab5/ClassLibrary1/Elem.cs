﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Elem
    {
        public int Value { get; set; }
        public Elem Prev { get; set; }
        public Elem Next { get; set; }

        public Elem(int value)
        {
            Value = value;
        }
    }
}