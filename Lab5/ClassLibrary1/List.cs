﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class List
    {
        public Elem Head = null;
        public Elem Tail = null;
        public int Count = 0;

        public List()
        {
            Head = null;
            Tail = null;
        }

        public void AddElement(List list, int value)
        {
            Elem Element = new Elem(value);
            Element.Next = null;
            Element.Prev = list.Tail;
            if (list.Count != 0)
            {
                list.Tail.Next = Element;
            }
            else
            {
                list.Head = Element;
            }
            list.Tail = Element;
            list.Count++;
        }

        public void PrintList(List list)
        {
            Elem Element = list.Head;

            if (list.Count == 0)
            {
                return;
            }

            while (Element != null)
            {
                Console.Write($"{Element.Value}\t");
                Element = Element.Next;
            }
            Console.WriteLine();
        }

        public Elem GetElementAt(List list, int index)
        {
            Elem Element = null;
            int i;

            if (index >= list.Count)
            {
                return null;
            }

            if (index < list.Count / 2)
            {
                i = 0;
                Element = list.Head;
                while (Element != null && i < index)
                {
                    Element = Element.Next;
                    i++;
                }
            }
            else
            {
                i = list.Count - 1;
                Element = list.Tail;
                while (Element != null && i > index)
                {
                    Element = Element.Prev;
                    i--;
                }
            }
            return Element;
        }

        public void Swap(List list, Elem a, Elem b)
        {
            Elem temp = new Elem(0);
            if (a.Next == b && b.Prev == a)
            {
                if (b.Next != null)
                {
                    b.Next.Prev = a;
                }
                else
                {
                    list.Tail = a;
                }
                a.Next = b.Next;
                b.Next = a;

                if (a.Prev != null)
                {
                    a.Prev.Next = b;
                }
                else
                {
                    list.Head = b;
                }
                b.Prev = a.Prev;
                a.Prev = b;
            }
            else
            {
                if (a.Prev != null)
                {
                    a.Prev.Next = b;
                }
                else
                {
                    list.Head = b;
                }

                if (b.Next != null)
                {
                    b.Next.Prev = a;
                }
                else
                {
                    list.Tail = a;
                }

                a.Next.Prev = b;

                b.Prev.Next = a;

                temp = a.Prev;
                a.Prev = b.Prev;
                b.Prev = temp;

                temp = a.Next;
                a.Next = b.Next;
                b.Next = temp;
            }
        }
    }
}