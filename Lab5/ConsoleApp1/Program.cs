﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Stopwatch Choose = new Stopwatch();
            Stopwatch Insert = new Stopwatch();
            Stopwatch Shella = new Stopwatch();
            Stopwatch InsertList = new Stopwatch();
            Random rand = new Random();
            Console.Write("Введите количество элементов: ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = rand.Next(0, 101);
            }
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                arr[i] = array[i];
            }
            Choose.Start();
            SortChoise(ref arr);
            Choose.Stop();
            for (int i = 0; i < n; i++)
            {
                arr[i] = array[i];
            }
            Insert.Start();
            SortInsert(ref arr);
            Insert.Stop();
            for (int i = 0; i < n; i++)
            {
                arr[i] = array[i];
            }
            Shella.Start();
            SortShella(ref arr);
            Shella.Stop();
            List list = CreateRandomList();
            InsertList.Start();
            SortInsertList(ref list);
            InsertList.Stop();
            list.PrintList(list);
            TimeSpan choise = Choose.Elapsed;
            TimeSpan insert = Insert.Elapsed;
            TimeSpan shella = Shella.Elapsed;
            TimeSpan insertList = InsertList.Elapsed;
            double choiseTime = choise.TotalMilliseconds;
            double insertTime = insert.TotalMilliseconds;
            double shellaTime = shella.TotalMilliseconds;
            double insertListTime = insertList.TotalMilliseconds;
            Console.Clear();
            Console.WriteLine($"Количество элементов: {n}");
            Console.WriteLine("Время работы метода выбора: " + choiseTime);
            Console.WriteLine("Время работы метода вставками: " + insertTime);
            Console.WriteLine("Время работы метода  Шелла: " + shellaTime);
            Console.WriteLine("Время работы метода  вставками(списком): " + insertListTime);
            Console.ReadKey();
        }

        static void SortChoise(ref int[] arr)
        {
            int min = (int)1e7;
            int mini = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = i; j < arr.Length; j++)
                {
                    if (min > arr[j])
                    {
                        min = arr[j];
                        mini = j;
                    }
                }
                min = (int)1e7;
                int temp = arr[i];
                arr[i] = arr[mini];
                arr[mini] = temp;
            }
        }

        static void SortInsert(ref int[] arr)
        {
            int x, j;
            for (int i = 0; i < arr.Length; i++)
            {
                x = arr[i];
                for (j = i - 1; j >= 0 && arr[j] > x; j--)
                {
                    arr[j + 1] = arr[j];
                }
                arr[j + 1] = x;
            }
        }

        static void SortShella(ref int[] arr)
        {
            List<int> inc = Inc(arr.Length);
            int s = inc.Count - 1;
            while (s > 0)
            {
                for (int i = inc[s]; i < arr.Length; i++)
                {
                    for (int j = i - inc[s]; j >= 0 && arr[j] > arr[j + inc[s]]; j -= inc[s])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + inc[s]];
                        arr[j + inc[s]] = temp;
                    }
                }
                s--;
            }
        }

        static List<int> Inc(int length)
        {
            List<int> inc = new List<int>();
            int s = -1;
            do
            {
                s++;
                if (s % 2 == 0)
                {
                    inc.Add((int)(9 * Math.Pow(2, s) - 9 * Math.Pow(2, s / 2) + 1));
                }
                else
                {
                    inc.Add((int)(8 * Math.Pow(2, s) - 6 * Math.Pow(2, (s + 1) / 2) + 1));
                }
            } while (3 * inc[s] < length);
            return inc;
        }

        static void SortInsertList(ref List list)
        {
            for (int i = 1; i < list.Count; i++)
            {
                if (list.GetElementAt(list, i - 1).Value > list.GetElementAt(list, i).Value)
                {
                    for (int j = i; j > 0; j--)
                    {

                        if (list.GetElementAt(list, j - 1).Value < list.GetElementAt(list, j).Value)
                        {
                            break;
                        }
                        list.Swap(list, list.GetElementAt(list, j - 1), list.GetElementAt(list, j));
                    }
                }
            }
        }

        static List CreateRandomList()
        {
            Random rand = new Random();
            List list = new List();
            for (int i = 0; i < 1000; i++)
            {
                list.AddElement(list, rand.Next(0, 51));
            }
            list.PrintList(list);
            return list;
        }
    }
}