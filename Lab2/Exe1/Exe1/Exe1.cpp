﻿#include <stdio.h>
#include <math.h>
#include <ctime>
#include <Windows.h>
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	unsigned int x = time(0), m = pow(2, 31) - 1, c = 2147483587, a = 2147483629, xn, n, mas[4000];
	int k = 0;
	float f, g = 0, w = 0;
	for (int i = 0; i < 4000; i++) {
		xn = ((a * x + c) % m);
		x = xn;
		n = (short)(1.0 * xn * 151 / (m - 1));
		printf("%hd\t", n);
		mas[i] = n;

	}
	printf("\n");
	for (int q = 0; q <= 150; q++) {
		k = 0;
		for (int i = 0; i < 4000; i++) {
			if (mas[i] == q) k++;
		}
		f = k / 4000.0;
		g += 1.0 * q * f;
		printf("Частота интервалов для числа %d = %d\n", q, k);
		printf("Статистическая вероятность для числа %d = %f\n", q, f);
	}
	printf("Математическое ожидание случайных величин = %f\n", g);
	for (int q = 0; q <= 150; q++)
	{
		k = 0;
		for (int i = 0; i < 4000; i++) {
			if (mas[i] == q) k++;
		}
		f = k / 4000.0;
		w += pow((q - g), 2) * f;
	}
	printf("Дисперсия случайных величин = %f\n", w);
	printf("Среднее квадратическое отклонение случайных величин = %f", sqrt(w));
	return 0;
}