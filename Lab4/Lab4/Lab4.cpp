﻿#include <iostream>
#include <windows.h>
#include <math.h>
#include <time.h>

double f(double x)
{
	double f;
	f = pow(sin(x), 2) + 3;
	return f;
}
int main()
{
	srand(time(NULL));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int a, b, n;
	printf("Введите левый край:");
	scanf_s("%d", &a);
	printf("Введите правый край:");
	scanf_s("%d", &b);
	printf("Введите количество проходов:");
	scanf_s("%d", &n);
	double h;
	h = 1.0 * (b - a) / n;
	double integral = 0;
	for (int i = 1; i < n; i++) {
		integral += f(a + (i - 0.5) * h);
	}
	integral *= h;
	printf("\nМетод прямоугольников\nИнтеграл = %lf", integral);
	integral = 0;
	double max = -1e7;
	for (int i = 1; i < n - 1; i++) {
		integral += f(a + i * h);
		if (integral > max) {
			max = integral;
		}

	}
	integral = (h / 2) * ((f(a) + f(b) + 2 * integral));
	printf("\nМетод трапеции\nИнтеграл = %lf", integral);
	integral = 0;
	double integral1 = 0;
	for (int i = 1; i < n; i++) {
		if (i % 2 != 0)
			integral += f(a + i * h);
	}
	for (int i = 1; i < n - 1; i++) {
		if (i % 2 == 0)
			integral1 += f(a + i * h);
	}
	integral = (h / 3) * ((f(a) + f(b) + 4 * integral + 2 * integral1));
	printf("\nМетод Симпсона\nИнтеграл = %lf", integral);
	double integralx, integraly;
	int c = max + 5;
	int k = 0;
	for (int i = 0; i < n; i++) {
		integralx = (a + rand() % (b - a + 1)) / 1.0;
		integraly = (rand() % c) / 1.0;
		if (integraly > f(integralx)) k++;
	}
	double f;
	f = 1.0 * k / n;
	double w;
	w = (b - a) * c * f / n;
	printf("\nМетод Монте-Карло\nИнтеграл = %lf", w);
}
